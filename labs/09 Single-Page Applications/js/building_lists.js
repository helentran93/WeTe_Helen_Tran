
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

//tehtävä 1, unordered list
/*var list = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title + ", " + books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);*/

//tehtävä 2 & 3, table & column-headings
var head = document.createElement('h1');
var table = document.createElement('table');
var title = document.createElement('th');
var ytitle = document.createElement('th');
var tbody = document.createElement('tbody');

title.innerHTML = "Title";
ytitle.innerHTML = "Year";

table.appendChild(title);
table.appendChild(ytitle);

for (var i=0; i < books.length; i++) {
	console.log(books[i].title);

	var row = document.createElement('tr');
	var item = document.createElement('td');
	var year = document.createElement('td');
	
	year.innerHTML = books[i].year;
	item.innerHTML = books[i].title;
	
	row.appendChild(item);
	row.appendChild(year);
	tbody.appendChild(row);
	table.appendChild(tbody);
	
}
document.body.appendChild(head);
document.body.appendChild(table);

//tehtävä 4, an event handler
function rowValue() {
	var rows = table.getElementsByTagName('tr');
	
	for(i=0; i < rows.length; i++) {
		var current = table.rows[i];
		var clickValue = function(row) {
			return function() {
				head.innerHTML = row.innerHTML;
			};
		};
		current.onclick =clickValue(current);
	}
}
window.onload = rowValue();
