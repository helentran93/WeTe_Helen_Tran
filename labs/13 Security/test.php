<?php
$string = "I'll \"walk\" the <b>dog</b> now";  // notice \-sign before double quotes!

$a = htmlentities($string);
$b = html_entity_decode($string);
$c = htmlspecialchars($string);
$d = strip_tags($string);

echo $a.", ".$b.", ".$c.", ".$d."<br>";

//esimerkki PHP sivulta
$text = '<p>Test paragraph.</p><!-- Comment --> <a href="#fragment">Other text</a>';
echo strip_tags($text);
echo "\n";

// Allow <p> and <a>
echo strip_tags($text, '<p><a>')."<br>";

//Oma esimerkki, antaa b-tagin näkyä
$example ='<p><b>No</b>, I shall not let thou pass!</p>';
echo strip_tags($example, '<b>')."<br>";

//onmouseover tagissa
$mouse = '<p onmouseover="hello()" id="hi"><b>Hello! Hover me!</b></p><script>function hello() {document.getElementById("hi").innerHTML = "Gotcha!";}</script>';
echo html_entity_decode($mouse);

//restricting onmouseover
$replacement = '';
$pattern = '/onmouseover="hello()"/';
echo preg_replace($pattern, $replacement, $mouse);

?>