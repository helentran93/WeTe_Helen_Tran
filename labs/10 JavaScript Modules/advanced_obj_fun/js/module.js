(function(window) {
    'use strict';
    
    function $(id) {
        this.id = function(id){
            return document.getElementById(id);
        };
    }
    
    // Export to window
	window.app = window.app || {};
	window.app.$ = $;
})(window);