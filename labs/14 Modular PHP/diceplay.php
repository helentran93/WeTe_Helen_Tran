<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$bias = $_GET["bias"];
$material = $_GET["material"];

$results = array();

// make a new dice
//$dice = new Dice($faces, $bias);
$m = new PhysicalDice($material, $faces);
for ($i = 1; $i<=$throws; $i++) {
    if ($bias != "") {
        $res = $m->probs($faces, $bias);
    }
    else {
        $res = $m->cast();
    }
    $results[] = array('id' => strval($i), 'res' => strval($res));
}

//counts, how many times each eye of the dice has shown up
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array('eyes' => strval($i), 'frequency' => strval($m->getFreq($i)));
}

//tehtävä 1 & 2
$average = array();
$values = array();
$values = array_column($results, 'res');
$average = array($m->averEyes($values));

//tehtävä 4
$materials = array('material' => $m->setMaterial($material));


//If the bias is not provided then it is treated as "unbiased" and the material as "null"
echo json_encode(array('faces'=>$faces, 'results'=>$results, 'frequencies'=>$freqs, 'average number of eye'=>$average, 'material of the dice' => $materials, 'bias'=>$bias));

?>