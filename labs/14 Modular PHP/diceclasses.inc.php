<?php

class Dice {
    private  $faces;
    private  $p;
    private  $freqs = array();
    
    // Constructor
    public function __construct($faces) {
        $this->faces = $faces;
    }
    
    public function cast() {
        $res = rand(1,$this->faces);
        $this->freqs[$res]++;
        return $res;
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    //tehtävä 1 & 2
    public function averEyes(&$eye) {
        $sum = 0;
        foreach($eye as $key=>$result) {
            $sum += intval($result);
        }
        $average = $sum / count($eye);
        return $average;
    }
    
    //tehtävä 3
    public function probs($faces, $bias) {
        $random = rand(1,100);
        $p = $bias * 100;
        $res;
        if ($p <= $random) {
            $res = $faces;
        }
        else {
            $res = rand(1,$this->faces-1);
        }
        $this->freqs[$res]++;
        return $res;
    }
}

//tehtävä 4
class PhysicalDice extends Dice {
    private $material;
    
    public function __construct($material, $faces) {
        $this->material = $material;
        parent::__construct($faces);
    }
    
    public function setMaterial($material) {
        if($material == "") {
            $material = null;
        }
        return $material;
    }
}

?>