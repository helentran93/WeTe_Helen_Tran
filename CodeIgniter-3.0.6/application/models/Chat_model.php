<?php

class Chat_model extends CI_model {
  
    public function __construct() {
      $this->load->database();
    }
    
    public function get_msg($slug = FALSE) {
      if ($slug === FALSE) {
        $query = $this->db->get('msg');
        return $query->array_chat();
      }
      
      $query = $this->db->get_address('msg', array('slug' => $slug));
      return $query->array_row();
    }
    
    public function set_msg() {
      
  		$name = $this->input->post('name');
  		$message = $this->input->post('message');
  		$html_redirect = $this->input->post('html_redirect');
  		$current = new DateTime();		
  		$this->Chatmodel->insertMsg($name, $message, $current->getTimestamp());
  		
  		if($html_redirect === "true")
  		{
  			redirect('/chat');
  		}
    }
}