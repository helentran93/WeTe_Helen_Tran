var notes = (function() {
    var list = [];
    
    return {
        add: function(note) {
            if(note && note.trim()){
                var item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
            }
        return false;
    },
      remove: function(index) {
          if(index > -1) {
              list.splice(index, 1);
              return true;
          } else{
              return false;
          }
      },
      count: function(note) {
          return list.length;
      },
      list: function() {},
      find: function(str) {
          for(var i = 0; i < list.length; i++) {
              if(list[i] == str) {
                  return true;
              }
          }
          return false;
      },
      clear: function() {
          list.splice(0, list.length);
      }
    };
}());